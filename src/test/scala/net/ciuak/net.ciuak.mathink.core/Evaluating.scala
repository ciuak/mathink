package net.ciuak.mathink

import net.ciuak.mathink.core._
import Implicits._
import org.scalatest.{FlatSpec, Matchers}

class Evaluating extends FlatSpec with Matchers {

  "Evaluator" should "give 4 from 2+2" in {
    Add(Const(2), Const(2)).calculate(new Context(Array())) shouldEqual new Rational(4)
  }

  it should "give 15 from 1+2+3+4+5" in {
    Add(Add(Add(Add(Const(1), Const(2)), Const(3)), Const(4)), Const(5)).calculate(new Context(Array())) shouldEqual new Rational(15)
  }

  it should "give 5 from 4*7/5" in {
    Divide(Multiply(Const(4), Const(7)), Const(5)).calculate(new Context(Array())) shouldEqual new Rational(BigInt(28), BigInt(5))
  }

  it should "give 49 from x->(x*x)$7" in {
    Call(Const(new Lambda(Multiply(Symbol("x"), Symbol("x")), "x")), Const(7)).calculate(new Context(Array())) shouldEqual new Rational(49)
  }

}
