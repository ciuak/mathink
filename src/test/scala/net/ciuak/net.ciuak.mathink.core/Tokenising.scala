package net.ciuak.mathink

import net.ciuak.mathink.core._
import org.scalatest.{FlatSpec, Matchers}

class Tokenising extends FlatSpec with Matchers {

  "Tokeniser" should "not fail in a very basic test" in {
    Tokeniser("2+2") shouldEqual Array(Number("2"), Addition, Number("2"))
  }

  it should "recognise '+' and '-'" in {
    Tokeniser("1+2-3") shouldEqual Array(Number("1"), Addition, Number("2"), Subtraction, Number("3"))
  }

  it should "recognise '*' and '/'" in {
    Tokeniser("5*45/33") shouldEqual Array(Number("5"), Multiplication, Number("45"), Division, Number("33"))
  }

  it should "recognise parenthesis" in {
    Tokeniser("()") shouldEqual Array(OpeningParen, ClosingParen)
  }

  it should "recognise lambda stuff" in {
    Tokeniser("->") shouldEqual Array(LambdaDef)
  }

  it should "recognise ^" in {
    Tokeniser("^") shouldEqual Array(Caret)
  }

}
