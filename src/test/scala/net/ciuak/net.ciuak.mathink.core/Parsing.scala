package net.ciuak.mathink

import net.ciuak.mathink.core._
import Implicits._
import org.scalatest.{FlatSpec, Matchers}

class Parsing extends FlatSpec with Matchers {

  "Parser" should "should not fail in a very basic test" in {
    Parser(Array(Number("2"), Addition,
      Number("2"))) shouldEqual Add(Const(2), Const(2))
  }

  it should "recognise '+' and '-'" in {
    Parser(Array(Number("1"), Addition, Number("2"), Subtraction,
      Number("3"))) shouldEqual Subtract(Add(Const(1), Const(2)), Const(3))
  }

  it should "recognise '*' and '/'" in {
    Parser(Array(Number("46"), Multiplication, Number("1"),
      Division, Number("4"))) shouldEqual Divide(Multiply(Const(46), Const(1)), Const(4))
  }

  it should "do order of operations involving '+', '-', '*' and '/'" in {
    Parser(Array(Number("3"), Addition, Number("5"), Multiplication, Number("7"),
      Subtraction, Number("1"), Division, Number("496"))) shouldEqual Subtract(Add(Const(3),
      Multiply(Const(5), Const(7))), Divide(Const(1), Const(496)))
  }

  it should "correctly parse nested parenthesis" in {
    Parser(Array(Number("2"), Addition, OpeningParen, Number("2"), Division, OpeningParen,
      Number("7"), ClosingParen, ClosingParen)) shouldEqual Add(Const(2), Divide(Const(2), Const(7)))
  }

  it should "fail when the parens are unbalanced" in {
    intercept[IllegalArgumentException] {
      Parser(Array(ClosingParen))
    }
    intercept[IllegalArgumentException] {
      Parser(Array(OpeningParen))
    }
  }

  it should "correctly parse statements with '->' without parens" in {
    Parser(Array(Number("3"), Multiplication, Identifier("x"), LambdaDef, Number("2"), Addition,
      Number("9"))) shouldEqual Multiply(Const(new Rational(3)), Const(new Lambda(Add(Const(2), Const(9)), "x")))
  }

  it should "correctly parse lambda calls" in {
    Parser(Array(OpeningParen, Identifier("a"), LambdaDef, Identifier("a"), Number("4"), ClosingParen, OpeningParen, Identifier("a"),
      LambdaDef, Identifier("a"), Multiplication, Number("11"), ClosingParen)) shouldEqual Call(
        Const(new Lambda(Call(Symbol("a"), Const(new Rational(4))), "a")),
        Const(new Lambda(Multiply(Symbol("a"), Const(new Rational(11))), "a"))
    )
  }

}
