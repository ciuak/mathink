package net.ciuak.mathink.functions

import net.ciuak.mathink.core._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class PrimeCalc extends Value {
  override def toString = "<nth_prime>"
  override def equals(that : Value) = that.isInstanceOf[PrimeCalc]

  private def get_prime(int : BigInt) : BigInt = {
    var cter = BigInt(1)
    var elps = 0
    while(elps < int) {
      cter += 1
      if(is_prime(cter)) elps += 1
    }
    cter
  }

  private def is_prime(int : BigInt) : Boolean = {
    if(int <= 1) return false
    var ct = 2
    while(ct <= int/2) {
      if(int % ct == BigInt(0))
        return false
      ct += 1
    }
    true
  }

  override def call(that : Expression, ctx : Context) : Value = {
    val v = Await.result(that.evaluate(ctx), Duration.Inf)
    v match {
      case r : Rational =>
        if(r.isInteger && r.num > 0)
          new Rational(get_prime(r.toBigInt))
        else
          new ErrorValue("expected a positive integer")
    }
  }
}
