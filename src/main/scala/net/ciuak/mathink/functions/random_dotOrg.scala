package net.ciuak.mathink.functions

import dispatch.Defaults._
import dispatch._
import net.ciuak.mathink.core._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class random_dotOrg extends Value {
  override def equals(that : Value) = that.isInstanceOf[random_dotOrg]
  override def toString = "<randomorg>"
  override def call(that : Expression, ctx : Context) : Value = {
    Await.result(that.evaluate(ctx), Duration.Inf) match {
      case r : Rational =>
        if(!r.isInteger || r.num < 0) new ErrorValue("a natural number was expected")
        else new random_dotOrg_I(r.num)
      case _ => new ErrorValue("a number was expected")
    }
  }

  private class random_dotOrg_I(val max : BigInt) extends Value {
    override def equals(that : Value) = that.isInstanceOf[random_dotOrg_I]
    override def toString = s"<random:0~$max>"
    override def call(that : Expression, ctx : Context) : Value = {
      try {
        new Rational(random_dotOrg_Helper.get_random_int(max))
      } catch {
        case c : ClassCastException => new ErrorValue("request error")
      }
    }
  }
}


private object random_dotOrg_Helper {
  val target_url = "https://api.random.org/json-rpc/1/invoke"
  def send_json_to_server(json : JsValue) : JsValue = {
    Await.result(Http(url(target_url).POST
                                     .setContentType("application/json", "utf-8")
                                     .<<(json.compactPrint)
      OK as.String), Duration.Inf).parseJson
  }
  def get_random_int(max : BigInt) : BigInt = {
    val obj = send_json_to_server(JsObject(
      "jsonrpc" -> JsString("2.0"),
      "method" -> JsString("generateIntegers"),
      "id" -> JsNumber(0),
      "params" -> JsObject(
        "apiKey" -> JsString(REPL.api_key),
        "n" -> JsNumber(1),
        "min" -> JsNumber(0),
        "max" -> JsNumber(max),
        "replacement" -> JsFalse
      )
    )).asJsObject
    obj.fields("result").asInstanceOf[JsObject].fields("random").asInstanceOf[JsObject].fields("data")
      .asInstanceOf[JsArray].elements(0).asInstanceOf[JsNumber].value.toBigInt()
  }
}