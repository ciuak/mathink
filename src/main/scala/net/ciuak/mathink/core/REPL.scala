package net.ciuak.mathink.core

import java.util.Scanner

import net.ciuak.mathink.functions.{PrimeCalc, random_dotOrg}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object REPL extends App {

  var s = new Scanner(System.in)
  var context = new Context(Array("nth_prime" -> new PrimeCalc, "randomorg" -> new random_dotOrg))
  var api_key = ""
  while(s.hasNextLine)
      try {
        val line = s.nextLine
        if(line matches "^\\)set_api_key [0-9a-f-]+$")
          api_key = line.drop(13)
        else {
          val toks = Tokeniser(line)
          val expr = Parser(toks, context)
          val eval = Await.result(expr.evaluate(context), Duration.Inf)
          println(eval)
          eval match {
            case Variable(name, value) =>
              context = context.update(name, value)
            case _ =>
          }
        }
      } catch {
        case x : InterruptedException => throw x
        case i : Exception =>
          i printStackTrace System.err
          System.err.flush()
      }

}
