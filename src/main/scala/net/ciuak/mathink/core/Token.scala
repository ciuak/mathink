package net.ciuak.mathink.core

trait Token     {}
trait Priority1 {}
trait Priority2 {}
trait Priority3 {}
trait Priority4 {}
trait Priority5 {}
trait Priority6 {}

case object Addition                  extends Token with Priority5
case object Subtraction               extends Token with Priority5
case object Multiplication            extends Token with Priority4
case object Division                  extends Token with Priority4
case object Caret                     extends Token with Priority3
case object LambdaDef                 extends Token with Priority2
case object OpeningParen              extends Token with Priority1
case object ClosingParen              extends Token with Priority1
case object Assignment                extends Token with Priority1
case class  Identifier(name : String) extends Token with Priority6
case class  Number(value : String)    extends Token
case class  Expr(expr : Expression)   extends Token