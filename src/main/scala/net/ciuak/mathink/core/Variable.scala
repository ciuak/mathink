package net.ciuak.mathink.core

case class Variable(name : String, value : Value) extends Value {

  override def toString = s"variable $name: $value"

}
