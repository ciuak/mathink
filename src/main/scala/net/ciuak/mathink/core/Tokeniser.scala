package net.ciuak.mathink.core

object Tokeniser {

  def apply(_expr : String) = {
    var leftover = _expr.trim
    var tokens = Array[Token]()
    while (leftover.length > 0) {
      if ("^[0-9]+".r.findFirstIn(leftover).isDefined) {
        val number = "^[0-9]+".r.findFirstIn(leftover).get
        leftover = leftover drop number.length
        tokens = tokens :+ Number(number)
      } else if ("^->".r.findFirstIn(leftover).isDefined) {
        tokens = tokens :+ LambdaDef
        leftover = leftover.drop(2)
      } else if(leftover(0) == ':') {
        tokens = tokens :+ Assignment
        leftover = leftover.tail
      } else if(leftover(0) == '^') {
        tokens = tokens :+ Caret
        leftover = leftover.tail
      } else if (leftover(0) == '+') {
        tokens = tokens :+ Addition
        leftover = leftover.tail
      } else if (leftover(0) == '-') {
        tokens = tokens :+ Subtraction
        leftover = leftover.tail
      } else if (leftover(0) == '*') {
        tokens = tokens :+ Multiplication
        leftover = leftover.tail
      } else if (leftover(0) == '/') {
        tokens = tokens :+ Division
        leftover = leftover.tail
      } else if (leftover(0) == '(') {
        tokens = tokens :+ OpeningParen
        leftover = leftover.tail
      } else if (leftover(0) == ')') {
        tokens = tokens :+ ClosingParen
        leftover = leftover.tail
      } else if ("^[A-Za-z_-][A-Za-z0-9_-]*($|[^A-Za-z0-9_-])".r.findFirstIn(leftover).isDefined) {
        val identifier = "^([A-Za-z_-][A-Za-z0-9_-]*)".r.findFirstIn(leftover).get
        leftover = leftover drop identifier.length
        tokens = tokens :+ Identifier(identifier)
      } else {
        throw new IllegalArgumentException("Token not supported")
      }
      leftover = leftover.trim
    }
    tokens
  }

}
