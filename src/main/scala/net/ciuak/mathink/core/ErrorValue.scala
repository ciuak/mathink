package net.ciuak.mathink.core

class ErrorValue private(val arg : Option[String]) extends Value {
  def this(s : String) = this(Some(s))
  def this() = this(None)
  override def equals(that : Value) : Boolean = false
  override def toString : String = "<E:" + arg.getOrElse("generic") + ">"
  override def +(that : Value) : Value = this
  override def -(that : Value) : Value = this
  override def *(that : Value) : Value = this
  override def /(that : Value) : Value = this
  override def ^(that : Value) : Value = this
  override def call(that : Expression, ctx : Context) : Value = this
}
