package net.ciuak.mathink.core

class Context(_bindings : Array[(String, Value)]) {

  val bindings : Array[(String, Value)] = _bindings

  def update(name : String, nval : Value) : Context = {
    val idx = bindings.zipWithIndex.find(_._1._1 == name)
    idx match {
      case Some(x) =>
        var b2 = bindings.clone()
        b2(x._2) = (name, nval)
        new Context(b2)
      case None =>
        new Context(bindings :+ (name, nval))
    }
  }

  def apply(name : String) : Value = {
    bindings.find(_._1 == name).get._2
  }

  def get(name : String) : Option[Value] = {
    try {
      Some(apply(name))
    } catch {
      case n : NoSuchElementException => None
    }
  }

  def equals(name : String, value : Value) : Boolean =
    get(name).isDefined && apply(name) == value

}
