package net.ciuak.mathink.core

class Lambda(_body : Expression, _bind : String) extends Value {

  val body : Expression = _body
  val bind : String = _bind

  override def toString = s"$bind -> $body"
  override def equals(that : Value) = that match {
    case l : Lambda => body == l.body && bind == l.bind
    case _ => false
  }

  override def call(that : Expression, ctx : Context) : Value = {
    val arg = that.calculate(ctx)
    val res = body.calculate(ctx(bind) = arg)
    res match {
      case l : Lambda => new Lambda(l.body.withIdentifierSubstituted(bind, arg), l.bind)
      case _ => res
    }
  }
  
}
