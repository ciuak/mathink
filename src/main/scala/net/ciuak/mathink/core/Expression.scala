package net.ciuak.mathink.core

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

trait Expression extends Cloneable {
  final def apply(ctx : Context) : Future[Value] = evaluate(ctx)
  def calculate(ctx : Context) : Value = Await.result(evaluate(ctx), Duration.Inf)
  def evaluate(ctx : Context) : Future[Value]
  def withIdentifierSubstituted(n : String, v : Value) : Expression
  def optimise : Expression = this
}

trait RequiresFuture {}

case class Add(left : Expression, right : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = left.evaluate(ctx)
    val r = right.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    val rf = Await.result(r, Duration.Inf)
    lf + rf
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Add(left.withIdentifierSubstituted(n, v), right.withIdentifierSubstituted(n, v))
  override def optimise = {
    val l = left.optimise
    val r = right.optimise
    if(l == Const(new Rational(0))) r
    else if(r == Const(new Rational(0))) l
    else Add(l, r)
  }
}

case class Subtract(left : Expression, right : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = left.evaluate(ctx)
    val r = right.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    val rf = Await.result(r, Duration.Inf)
    lf - rf
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Subtract(left.withIdentifierSubstituted(n, v), right.withIdentifierSubstituted(n, v))
  override def optimise = {
    val l = left.optimise
    val r = right.optimise
    if(r == Const(new Rational(0))) l
    else if(l == r) Const(new Rational(0))
    else Subtract(l, r)
  }
}

case class Multiply(left : Expression, right : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = left.evaluate(ctx)
    val r = right.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    val rf = Await.result(r, Duration.Inf)
    lf * rf
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Multiply(left.withIdentifierSubstituted(n, v), right.withIdentifierSubstituted(n, v))
  override def optimise = {
    val l = left.optimise
    val r = right.optimise
    if(l == Const(new Rational(1))) r
    else if(r == Const(new Rational(1))) l
    else if(l == Const(new Rational(0)) || r == Const(new Rational(0))) Const(new Rational(0))
    else Multiply(l, r)
  }
}

case class Divide(left : Expression, right : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = left.evaluate(ctx)
    val r = right.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    val rf = Await.result(r, Duration.Inf)
    lf / rf
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Divide(left.withIdentifierSubstituted(n, v), right.withIdentifierSubstituted(n, v))
  override def optimise = {
    val l = left.optimise
    val r = right.optimise
    if(r == Const(new Rational(1))) l
    else Divide(l, r)
  }
}

case class Power(left : Expression, right : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = left.evaluate(ctx)
    val r = right.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    val rf = Await.result(r, Duration.Inf)
    lf ^ rf
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Power(left.withIdentifierSubstituted(n, v), right.withIdentifierSubstituted(n, v))
  override def optimise = {
    val l = left.optimise
    val r = right.optimise
    if(r == Const(new Rational(1))) l
    else if(r == Const(new Rational(0))) Const(new Rational(1))
    else Power(l, r)
  }
}

case class Call(lambda : Expression, arg : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future {
    val l = lambda.evaluate(ctx)
    // val r = arg.evaluate(ctx)
    val lf = Await.result(l, Duration.Inf)
    // val rf = Await.result(r, Duration.Inf)
    lf.call(arg, ctx)
  }
  def withIdentifierSubstituted(n : String, v : Value) =
    Call(lambda.withIdentifierSubstituted(n, v), arg.withIdentifierSubstituted(n, v))
}

case class Const(v : Value) extends Expression {
  def evaluate(ctx : Context) = Future {v}
  def withIdentifierSubstituted(n : String, v : Value) : Expression = this
}

case class Symbol(name : String) extends Expression {
  def evaluate(ctx : Context) = Future { ctx(name) }
  def withIdentifierSubstituted(n : String, v : Value) : Expression =
    if(n == name) Const(v)
    else this
}

case class Assign(name : String, value : Expression) extends Expression with RequiresFuture {
  def evaluate(ctx : Context) = Future { new Variable(name, value.calculate(ctx)) }
  def withIdentifierSubstituted(n : String, v : Value) : Expression =
    throw new IllegalArgumentException("How can you make an assignment in a lambda? (It's impossible) I did not expect that! " +
      "This is very rude of you!")
}