package net.ciuak.mathink.core

object Parser {

  private def getProperToken(tokens : Array[Token]) = {
    val t = tokens.zipWithIndex
    t.find { x => x._1.isInstanceOf[Priority1] } match {
      case Some(x) => x._2
      case None => t.find { x => x._1.isInstanceOf[Priority2] } match {
        case Some(x) => x._2
        case None =>
          t.find { x => try { /* x._1.isInstanceOf[Expr] && x._1.asInstanceOf[Expr].expr.isInstanceOf[Const] &&
            x._1.asInstanceOf[Expr].expr.asInstanceOf[Const].v.isInstanceOf[Lambda] && */
            t(x._2 + 1)._1.isInstanceOf[Number] && t(x._2 + 1)._1.isInstanceOf[Expr] } catch {
              case e : ArrayIndexOutOfBoundsException => false
          } } match {
            case Some(x) => x._2
            case None => t.find { x => x._1.isInstanceOf[Priority3] } match {
              case Some(x) => x._2
              case None => t.find { x => x._1.isInstanceOf[Priority4] } match {
                case Some(x) => x._2
                case None => t.find { x => x._1.isInstanceOf[Priority5] } match {
                  case Some(x) => x._2
                  // case None => throw new IllegalArgumentException("Can't perform an operation")
                  case None => t.find { x => x._1.isInstanceOf[Priority6] } match {
                    case Some(x) => x._2
                    case None => t(0)._2
                  }
                }
              }
            }
        }
      }
    }
  }

  private def findParenPair(tokens : Array[Token], bindings : Context) : Array[Token] = {
    var parenStart = 0
    var parenCount = 0
    for(i <- 0 until tokens.length) {
      val current = tokens(i)
      if(current == OpeningParen) {
        if (parenCount == 0)
          parenStart = i
        parenCount += 1
      }
      if(current == ClosingParen)
        if(parenCount == 1) {
          val x = tokens.take(parenStart) ++ Array(Expr(Parser(tokens.slice(parenStart + 1, i), bindings))) ++ tokens.drop(i + 1)
          return x
        }
        else if(parenCount > 1)
          parenCount -= 1
        else
          throw new IllegalArgumentException("Unexpected closing paren in findParenPair")
    }
    throw new IllegalArgumentException("Something went wrong")
  }

  def apply(tokens : Array[Token]) : Expression = apply(tokens, new Context(Array()))

  def apply(tokens : Array[Token], context : Context) : Expression = {
    if(context.equals("debug", new Rational(1)))
      println("[Debug] " + tokens.mkString(", "))
    val assignment = tokens.zipWithIndex.find(_._1 == Assignment).getOrElse((null, -1))._2
    if(assignment == 1 && tokens(0).isInstanceOf[Identifier]) {
      return Assign(tokens(0).asInstanceOf[Identifier].name, Parser(tokens drop 2, context))
    } else if(assignment != -1) {
      throw new IllegalArgumentException("Assignment must be preceded by a symbol")
    }
    if(tokens.length == 1)
      tokens(0) match {
        case number : Number => return Const(new Rational(BigInt(number.value)))
        case expr : Expr => return expr.expr
        case _ =>
      }
    val idx = getProperToken(tokens)
    val tokenToArg : PartialFunction[Token, Expression] = {
      case n : Number     => Const(new Rational(BigInt(n.value)))
      case i : Identifier => Symbol(i.name)
      case e : Expr       => e.expr
    }
    var newTokens : Array[Token] = Array()
    if(tokens(idx) == OpeningParen)
      newTokens = findParenPair(tokens, context)
    else if(tokens(idx) == ClosingParen)
      throw new IllegalArgumentException("Unexpected closing paren")
    else {
      lazy val lop = tokenToArg(tokens(idx - 1))
      lazy val rop = tokenToArg(tokens(idx + 1))
      newTokens = tokens(idx) match {
        case Addition       => tokens.take(idx - 1) ++ Array(Expr(Add     (lop, rop))) ++ tokens.drop(idx + 2)
        case Subtraction    => tokens.take(idx - 1) ++ Array(Expr(Subtract(lop, rop))) ++ tokens.drop(idx + 2)
        case Multiplication => tokens.take(idx - 1) ++ Array(Expr(Multiply(lop, rop))) ++ tokens.drop(idx + 2)
        case Division       => tokens.take(idx - 1) ++ Array(Expr(Divide  (lop, rop))) ++ tokens.drop(idx + 2)
        case Caret          => tokens.take(idx - 1) ++ Array(Expr(Power   (lop, rop))) ++ tokens.drop(idx + 2)
        case LambdaDef      => tokens.take(idx - 1) ++
          Array(Expr(Const(new Lambda(Parser(tokens.drop(idx + 1), context), lop.asInstanceOf[Symbol].name))))
        case Expr(e) => tokens.take(idx) ++ Array(Expr(Call(e, rop))) ++ tokens.drop(idx + 2)
        case Identifier(n : String) => tokens.take(idx) ++ Array(Expr(Call(Symbol(n), rop))) ++ tokens.drop(idx + 2)
      }
    }
    if(newTokens.length == 1)
      newTokens(0).asInstanceOf[Expr].expr
    else
      Parser(newTokens, context)
  }

}
