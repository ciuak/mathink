package net.ciuak.mathink.core

object Implicits {

  implicit def int2Rational(i : Int) = new Rational(i, 1)

}
