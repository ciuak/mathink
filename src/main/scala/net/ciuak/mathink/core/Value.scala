package net.ciuak.mathink.core

trait Value {

  final def ==(that : Value) = equals(that)

  def equals(that : Value) : Boolean =
    throw new IllegalArgumentException(s"Can't compare ${this.getClass.getName} with ${that.getClass.getName}")

  override def toString : String = "[value]"

  def +(that : Value) : Value =
    throw new IllegalArgumentException(s"Can't invoke + on ${this.getClass.getName}")

  def -(that : Value) : Value =
    throw new IllegalArgumentException(s"Can't invoke - on ${this.getClass.getName}")

  def *(that : Value) : Value =
    throw new IllegalArgumentException(s"Can't invoke * on ${this.getClass.getName}")

  def /(that : Value) : Value =
    throw new IllegalArgumentException(s"Can't invoke / on ${this.getClass.getName}")

  def ^(that : Value) : Value =
    throw new IllegalArgumentException(s"Can't invoke ^ on ${this.getClass.getName}")

  def call(that : Expression, ctx : Context) : Value =
    throw new IllegalArgumentException(s"${this.getClass.getName} can't be called")

}
