package net.ciuak.mathink.core

class Rational(_num : BigInt, _denom : BigInt) extends Value {

  val (num, denom) = reduce(_num, _denom)

  private def reduce(n : BigInt, d : BigInt) : (BigInt, BigInt) = {
    val g = n gcd d
    (n / g, d / g)
  }

  def isInteger = denom == BigInt(1)
  private def floor = new Rational(num / denom, BigInt(1))
  def toBigInt = num / denom
  def nonzero = num != BigInt(0)

  override def equals(that : Any) = that match {
    case r : Rational => num == r.num && denom == r.denom
    case b : BigInt   => num % denom == BigInt(0) && num / denom == b
    case i : Int      => num % denom == BigInt(0) && num / denom == BigInt(i)
    case _            => false
  }

  def this(x : BigInt) = this(x, 1)
  def this(_num : Value, _denom : Value) = this(
    _num.asInstanceOf[Rational].num * _denom.asInstanceOf[Rational].denom,
    _denom.asInstanceOf[Rational].num * _num.asInstanceOf[Rational].denom)

  override def toString =
    if(this.isInteger) s"${num / denom}"
    else s"$num/$denom"

  override def equals(that : Value) = that match {
    case r : Rational => num == r.num && denom == r.denom
    case _ => false
  }

  override def +(that : Value) : Rational = that match {
    case x : Rational => new Rational(num * x.denom + x.num * denom, denom * x.denom)
  }
  override def -(that : Value) : Rational = that match {
    case x : Rational => new Rational(num * x.denom - x.num * denom, denom * x.denom)
  }
  override def *(that : Value) : Rational = that match {
    case x : Rational => new Rational(num * x.num, denom * x.denom)
  }
  override def /(that : Value) : Rational = that match {
    case x : Rational => new Rational(num * x.denom, x.num * denom)
  }

  def square : Rational = this * this

  override def ^(that : Value) : Rational = that match {
    case exponent : Rational =>
      if(exponent.toBigInt < BigInt(0))
        (new Rational(1) / this) ^ (new Rational(-1) * exponent)
      else if(exponent.toBigInt == BigInt(0))
        new Rational(1)
      else if(exponent.toBigInt == BigInt(1))
        this
      else {
//        var exponent = x.toBigInt + 1
//        var result = new Rational(1)
//        while(exponent > BigInt(0)) {
//          val biggest_power = (Math.log(exponent.toDouble) / Math.log(2)).toInt - 1
//          result = result * this.pow2(biggest_power)
//          exponent -= new Rational(2).pow2(biggest_power - 1).toBigInt
//        }
//        result
        var result = new Rational(1)
        var counter = exponent.toBigInt
        while(counter > 0) {
          result = result * this
          counter -= 1
        }
        result
      }
  }

  private def pow2(exp : Int) : Rational = {
    if(exp > 1)
      this.square pow2 (exp - 1)
    else if(exp == 1)
      this.square
    else this
  }

}
